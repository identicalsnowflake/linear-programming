{-# LANGUAGE MagicHash #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UnboxedTuples #-}

-- | Low-level C bindings to glpk

module LP.CBindings
  ( type GLPK
  , type Managed
  , type Unmanaged
  , RVar(..)
  , CVar(..)
  -- , unsafeNewGLPKV -- 1-based vector allocation
  , newManagedGLPK
  , newUnmanagedGLPK
  , freeUnmanaged
  , get_num_rows
  , get_num_cols
  , add_rows
  , add_cols
  , set_row_bounds
  , set_col_bounds
  , get_obj_coef
  , set_obj_coef
  -- , set_mat_row
  -- , set_mat_col
  -- , load_matrix
  -- , del_rows
  -- , del_cols
  , copy_prob
  , erase_prob
  , set_row_stat
  , set_col_stat
  , std_basis
  , adv_basis
  , cpx_basis
  , get_status
  , get_primal_status
  , get_obj_val
  , get_row_stat
  , get_row_prim
  , get_row_dual
  , get_col_stat
  , get_col_prim
  , get_col_dual
  , solve
  , assign_kmul
  , assign_add
  , assert_rel

  , type BD
  , pattern BD_FREE
  , pattern BD_LOWER
  , pattern BD_UPPER
  , pattern BD_BOTH
  , pattern BD_FIXED

  , type OnOff
  , pattern On
  , pattern Off

  , type STAT(..)
  , pattern STAT_BS
  , pattern STAT_NL
  , pattern STAT_NU
  , pattern STAT_NF
  , pattern STAT_NS

  , type S_ERR
  , pattern S_ERR_OK
  , pattern S_ERR_INVALID_BASIS
  , pattern S_ERR_SINGULAR_MATRIX
  , pattern S_ERR_ILL_CONDITIONED
  , pattern S_ERR_INVALID_BOUNDS
  , pattern S_ERR_FAIL
  , pattern S_ERR_OBJ_LL
  , pattern S_ERR_OBJ_UL
  , pattern S_ERR_ITER_LIM
  , pattern S_ERR_TIMEOUT
  , pattern S_ERR_NO_PRIMAL_FEASIBLE
  , pattern S_ERR_NO_DUAL_FEASIBLE
  , pattern S_ERR_ROOT
  , pattern S_ERR_STOP
  , pattern S_ERR_MIPGAP
  , pattern S_ERR_NO_PRIMAL_DUAL_FEASIBLE
  , pattern S_ERR_NO_CONVERGENCE
  , pattern S_ERR_NUMERICAL_INSTABILITY
  , pattern S_ERR_INVALID_DATA
  , pattern S_ERR_RANGE

  , type SOL_STATUS
  , pattern SOL_UNDEFINED
  , pattern SOL_FEASIBLE
  , pattern SOL_INFEASIBLE
  , pattern SOL_NO_FEASIBLE
  , pattern SOL_OPTIMAL
  , pattern SOL_UNBOUNDED
  ) where

import Control.Monad.Primitive
import Data.AffineSpace
-- import Data.Primitive.ByteArray
-- import qualified Data.Vector.Primitive.Mutable as MPR
import Foreign.C.Types
import Foreign.ForeignPtr
import Foreign.Ptr
import GHC.Exts
import GHC.Int
import EShare.Hashable128 (Hashable128)


newtype RVar = RVar CInt deriving newtype (Eq,Hashable128)

instance AffineSpace RVar where
  type Diff RVar = Int
  {-# INLINE (.+^) #-}
  (.+^) (RVar (CInt (I32# x))) i = case i + I# (int32ToInt# x) of
    I# r -> RVar (CInt (I32# (intToInt32# r)))
  {-# INLINE (.-.) #-}
  (.-.) (RVar r1) (RVar r2) = case r1 - r2 of
    CInt (I32# i) -> I# (int32ToInt# i)

newtype CVar = CVar CInt deriving newtype (Eq,Hashable128,Show)

instance AffineSpace CVar where
  type Diff CVar = Int
  {-# INLINE (.+^) #-}
  (.+^) (CVar (CInt (I32# x))) i = case i + I# (int32ToInt# x) of
    I# r -> CVar (CInt (I32# (intToInt32# r)))
  {-# INLINE (.-.) #-}
  (.-.) (CVar r1) (CVar r2) = case r1 - r2 of
    CInt (I32# i) -> I# (int32ToInt# i)

newtype Managed s = Managed (ForeignPtr GLPK_R)
newtype Unmanaged s = Unmanaged (Ptr GLPK_R)

data GLPK_R

foreign import ccall unsafe "static glpk.h glp_create_prob" glp_new :: IO (Ptr GLPK_R)
foreign import ccall unsafe "static glpk.h glp_get_num_rows" glp_get_num_rows :: Ptr GLPK_R -> IO CInt
foreign import ccall unsafe "static glpk.h glp_get_num_cols" glp_get_num_cols :: Ptr GLPK_R -> IO CInt
foreign import ccall unsafe "static glpk.h glp_add_rows" glp_add_rows :: Ptr GLPK_R -> CInt -> IO CInt
foreign import ccall unsafe "static glpk.h glp_add_cols" glp_add_cols :: Ptr GLPK_R -> CInt -> IO CInt
foreign import ccall unsafe "static glpk.h glp_set_row_bnds" glp_set_row_bnds ::
  Ptr GLPK_R -> CInt -> BD -> Double -> Double -> IO ()
foreign import ccall unsafe "static glpk.h glp_set_col_bnds" glp_set_col_bnds ::
  Ptr GLPK_R -> CInt -> BD -> Double -> Double -> IO ()
foreign import ccall unsafe "static glpk.h glp_get_obj_coef" glp_get_obj_coef ::
  Ptr GLPK_R -> CInt -> IO Double
foreign import ccall unsafe "static glpk.h glp_set_obj_coef" glp_set_obj_coef ::
  Ptr GLPK_R -> CInt -> Double -> IO ()
-- foreign import ccall unsafe "static glpk.h glp_set_mat_row" glp_set_mat_row ::
--   Ptr GLPK_R -> CInt -> CInt -> MutableByteArray# s -> MutableByteArray# s -> IO ()
-- foreign import ccall unsafe "static glpk.h glp_set_mat_col" glp_set_mat_col ::
--   Ptr GLPK_R -> CInt -> CInt -> MutableByteArray# s -> MutableByteArray# s -> IO ()
-- foreign import ccall unsafe "static glpk.h glp_load_matrix" glp_load_matrix ::
--   Ptr GLPK_R -> CInt -> MutableByteArray# s -> MutableByteArray# s -> MutableByteArray# s -> IO ()
-- foreign import ccall unsafe "static glpk.h glp_del_rows" glp_del_rows ::
--   Ptr GLPK_R -> CInt -> MutableByteArray# s -> IO ()
-- foreign import ccall unsafe "static glpk.h glp_del_cols" glp_del_cols ::
--   Ptr GLPK_R -> CInt -> MutableByteArray# s -> IO ()
foreign import ccall unsafe "static glpk.h glp_copy_prob" glp_copy_prob ::
  Ptr GLPK_R -> Ptr GLPK_R -> OnOff -> IO ()
foreign import ccall unsafe "static glpk.h glp_erase_prob" glp_erase_prob ::
  Ptr GLPK_R -> IO ()
foreign import ccall unsafe "static glpk.h glp_set_row_stat" glp_set_row_stat ::
  Ptr GLPK_R -> CInt -> STAT -> IO ()
foreign import ccall unsafe "static glpk.h glp_set_col_stat" glp_set_col_stat ::
  Ptr GLPK_R -> CInt -> STAT -> IO ()
foreign import ccall unsafe "static glpk.h glp_std_basis" glp_std_basis ::
  Ptr GLPK_R -> IO ()
foreign import ccall unsafe "static glpk.h glp_adv_basis" glp_adv_basis ::
  Ptr GLPK_R -> CInt -> IO ()
foreign import ccall unsafe "static glpk.h. glp_cpx_basis" glp_cpx_basis ::
  Ptr GLPK_R -> IO ()
-- foreign import ccall unsafe "static glpk.h glp_simplex" glp_simplex ::
--   Ptr GLPK_R -> Ptr SMCP_R -> IO CInt -- see helper @glpk_solve@
foreign import ccall unsafe "static glpk.h glp_get_status" glp_get_status ::
  Ptr GLPK_R -> IO SOL_STATUS
foreign import ccall unsafe "static glpk.h glp_get_prim_stat" glp_get_prim_stat ::
  Ptr GLPK_R -> IO SOL_STATUS
foreign import ccall unsafe "static glpk.h glp_get_obj_val" glp_get_obj_val ::
  Ptr GLPK_R -> IO Double
foreign import ccall unsafe "static glpk.h glp_get_row_stat" glp_get_row_stat ::
  Ptr GLPK_R -> CInt -> IO STAT
foreign import ccall unsafe "static glpk.h glp_get_row_prim" glp_get_row_prim ::
  Ptr GLPK_R -> CInt -> IO Double
foreign import ccall unsafe "static glpk.h glp_get_row_dual" glp_get_row_dual ::
  Ptr GLPK_R -> CInt -> IO Double
foreign import ccall unsafe "static glpk.h glp_get_col_stat" glp_get_col_stat ::
  Ptr GLPK_R -> CInt -> IO STAT
foreign import ccall unsafe "static glpk.h glp_get_col_prim" glp_get_col_prim ::
  Ptr GLPK_R -> CInt -> IO Double
foreign import ccall unsafe "static glpk.h glp_get_col_dual" glp_get_col_dual ::
  Ptr GLPK_R -> CInt -> IO Double
-- foreign import ccall unsafe "static glpk.h glp_get_unbnd_ray" glp_get_unbnd_ray ::
--   Ptr GLPK_R -> IO CInt
foreign import ccall unsafe "static glpk.h glp_delete_prob" glp_free :: Ptr GLPK_R -> IO ()
foreign import ccall unsafe "static glpk.h &glp_delete_prob" glp_free' :: FunPtr (Ptr GLPK_R -> IO ())


-- supplementary
foreign import ccall unsafe "static glpk_helper.h glpk_solve" glpk_solve ::
  Ptr GLPK_R -> CInt -> CInt -> IO S_ERR
foreign import ccall unsafe "static glpk_helper.h glpk_assign_kmul" glpk_assign_kmul ::
  Ptr GLPK_R -> CVar -> Double -> CVar -> IO ()
foreign import ccall unsafe "static glpk_helper.h glpk_assign_add" glpk_assign_add ::
  Ptr GLPK_R -> CVar -> CVar -> CVar -> IO ()
foreign import ccall unsafe "static glpk_helper.h glpk_assert_rel" glpk_assert_rel ::
  Ptr GLPK_R -> CInt -> CVar -> CVar -> IO ()

-- {-# INLINABLE unsafeNewGLPKV #-}
-- -- glpk uses 1-based indices, so allocate buffers that interact with it using this primitive:
-- unsafeNewGLPKV :: (PrimMonad m , MPR.Prim a) => Int -> m (MPR.MVector (PrimState m) a)
-- unsafeNewGLPKV l = MPR.drop 1 <$> MPR.unsafeNew (l + 1)

{-# INLINE newManagedGLPK #-}
-- | GLPK will be freed by the Haskell runtime's garbage collector automatically once it is no longer needed.
newManagedGLPK :: PrimMonad m => m (Managed (PrimState m))
newManagedGLPK = unsafeIOToPrim do
  Managed <$> (glp_new >>= newForeignPtr glp_free')

{-# NOINLINE newUnmanagedGLPK #-}
-- | GLPK must be freed manually. You may free it via @freeUnmanaged@.
newUnmanagedGLPK :: PrimMonad m => m (Unmanaged (PrimState m))
newUnmanagedGLPK = Unmanaged <$> unsafeIOToPrim glp_new

{-# INLINE freeUnmanaged #-}
-- | Deallocate all resources associated with the given unmanaged GLPK pointer. Further usage of this pointer is undefined.
freeUnmanaged :: PrimMonad m => Unmanaged (PrimState m) -> m ()
freeUnmanaged p = withGLPK p glp_free

class GLPK f where
  withGLPK :: PrimMonad m => f (PrimState m) -> (Ptr GLPK_R -> IO a) -> m a

instance GLPK Managed where
  {-# INLINE withGLPK #-}
  withGLPK (Managed d) f = unsafeIOToPrim $ withForeignPtr d f

instance GLPK Unmanaged where
  {-# INLINE withGLPK #-}
  withGLPK (Unmanaged p) f = unsafeIOToPrim $ f p

{-# INLINE get_num_rows #-}
get_num_rows :: (GLPK g , PrimMonad m) => g (PrimState m) -> m CInt
get_num_rows g = withGLPK g \p -> glp_get_num_rows p

{-# INLINE get_num_cols #-}
get_num_cols :: (GLPK g , PrimMonad m) => g (PrimState m) -> m CInt
get_num_cols g = withGLPK g \p -> glp_get_num_cols p

{-# INLINE add_rows #-}
add_rows :: (GLPK g , PrimMonad m) => g (PrimState m) -> Int -> m RVar
add_rows g (I# k) = withGLPK g \p -> coerce <$> glp_add_rows p (CInt (I32# (intToInt32# k)))

{-# INLINE add_cols #-}
add_cols :: (GLPK g , PrimMonad m) => g (PrimState m) -> Int -> m CVar
add_cols g (I# k) = withGLPK g \p -> coerce <$> glp_add_cols p (CInt (I32# (intToInt32# k)))

{-# INLINE set_row_bounds #-}
set_row_bounds :: (GLPK g , PrimMonad m) => g (PrimState m) -> RVar -> BD -> Double -> Double -> m ()
set_row_bounds g r bd lb ub = withGLPK g \p -> glp_set_row_bnds p (coerce r) bd lb ub

{-# INLINE set_col_bounds #-}
set_col_bounds :: (GLPK g , PrimMonad m) => g (PrimState m) -> CVar -> BD -> Double -> Double -> m ()
set_col_bounds g c bd lb ub = withGLPK g \p -> glp_set_col_bnds p (coerce c) bd lb ub

{-# INLINE get_obj_coef #-}
get_obj_coef :: (GLPK g , PrimMonad m) => g (PrimState m) -> CVar -> m Double
get_obj_coef g v = withGLPK g \p -> glp_get_obj_coef p (coerce v)

{-# INLINE set_obj_coef #-}
set_obj_coef :: (GLPK g , PrimMonad m) => g (PrimState m) -> CVar -> Double -> m ()
set_obj_coef g v c = withGLPK g \p -> glp_set_obj_coef p (coerce v) c

-- {-# INLINE set_mat_row #-}
-- set_mat_row :: (GLPK g , PrimMonad m) => g (PrimState m) -> RVar -> MPR.MVector (PrimState m) CVar -> MPR.MVector (PrimState m) Double -> m ()
-- set_mat_row g r (MPR.MVector 1 l (MutableByteArray cs)) (MPR.MVector 1 _ (MutableByteArray coes)) = withGLPK g \p -> glp_set_mat_row p (coerce r)
--   do case l - 1 of
--        I# last_ind -> CInt (I32# last_ind)
--   do cs
--   do coes
-- set_mat_row _ _ _ _ = error "set_mat_row: vectors must be 1-based"

-- {-# INLINE set_mat_col #-}
-- set_mat_col :: (GLPK g , PrimMonad m) => g (PrimState m) -> RVar -> MPR.MVector (PrimState m) CVar -> MPR.MVector (PrimState m) Double -> m ()
-- set_mat_col g r (MPR.MVector 1 l (MutableByteArray cs)) (MPR.MVector 1 _ (MutableByteArray coes)) = withGLPK g \p -> glp_set_mat_col p (coerce r)
--   do case l - 1 of
--        I# last_ind -> CInt (I32# last_ind)
--   do cs
--   do coes
-- set_mat_col _ _ _ _ = error "set_mat_col: vectors must be 1-based"

-- {-# INLINE load_matrix #-}
-- load_matrix :: (GLPK g , PrimMonad m) => g (PrimState m) -> MPR.MVector (PrimState m) RVar -> MPR.MVector (PrimState m) CVar -> MPR.MVector (PrimState m) Double -> m ()
-- load_matrix g (MPR.MVector 1 l (MutableByteArray rv)) (MPR.MVector 1 _ (MutableByteArray cv)) (MPR.MVector 1 _ (MutableByteArray coev)) = withGLPK g \p -> glp_load_matrix p
--   do case l - 1 of
--        I# last_ind -> CInt (I32# last_ind)
--   do rv
--   do cv
--   do coev
-- load_matrix _ _ _ _  = error "load_matrix: vectors must be 1-based"

-- {-# INLINE del_rows #-}
-- del_rows :: (GLPK g , PrimMonad m) => g (PrimState m) -> MPR.MVector (PrimState m) RVar -> m ()
-- del_rows g (MPR.MVector 1 l (MutableByteArray rs)) = withGLPK g \p ->
--   glp_del_rows p
--     do case l - 1 of
--          I# last_ind -> CInt (I32# last_ind)
--     do rs
-- del_rows _ _ = error "del_rows: vectors must be 1-based"

-- {-# INLINE del_cols #-}
-- del_cols :: (GLPK g , PrimMonad m) => g (PrimState m) -> MPR.MVector (PrimState m) RVar -> m ()
-- del_cols g (MPR.MVector 1 l (MutableByteArray rs)) = withGLPK g \p ->
--   glp_del_cols p
--     do case l - 1 of
--          I# last_ind -> CInt (I32# last_ind)
--     do rs
-- del_cols _ _ = error "del_cols: vectors must be 1-based"

{-# INLINE copy_prob #-}
copy_prob :: PrimMonad m => Managed (PrimState m) -> Managed (PrimState m) -> OnOff -> m ()
copy_prob (Managed dest_p) (Managed source_p) oo =
  unsafeIOToPrim $ withForeignPtr dest_p \p1 ->
    withForeignPtr source_p \p2 ->
      glp_copy_prob p1 p2 oo

{-# INLINE erase_prob #-}
erase_prob :: (GLPK g , PrimMonad m) => g (PrimState m) -> m ()
erase_prob g = withGLPK g glp_erase_prob

{-# INLINE set_row_stat #-}
set_row_stat :: (GLPK g , PrimMonad m) => g (PrimState m) -> RVar -> STAT -> m ()
set_row_stat g r s = withGLPK g \p -> glp_set_row_stat p (coerce r) s

{-# INLINE set_col_stat #-}
set_col_stat :: (GLPK g , PrimMonad m) => g (PrimState m) -> CVar -> STAT -> m ()
set_col_stat g r s = withGLPK g \p -> glp_set_col_stat p (coerce r) s

{-# INLINE std_basis #-}
std_basis :: (GLPK g , PrimMonad m) => g (PrimState m) -> m ()
std_basis g = withGLPK g glp_std_basis

{-# INLINE adv_basis #-}
adv_basis :: (GLPK g , PrimMonad m) => g (PrimState m) -> m ()
adv_basis g = withGLPK g \p -> glp_adv_basis p 0

{-# INLINE cpx_basis #-}
cpx_basis :: (GLPK g , PrimMonad m) => g (PrimState m) -> m ()
cpx_basis g = withGLPK g glp_cpx_basis

{-# INLINE get_status #-}
get_status :: (GLPK g , PrimMonad m) => g (PrimState m) -> m SOL_STATUS
get_status g = withGLPK g glp_get_status

{-# INLINE get_primal_status #-}
get_primal_status :: (GLPK g , PrimMonad m) => g (PrimState m) -> m SOL_STATUS
get_primal_status g = withGLPK g glp_get_prim_stat

{-# INLINE get_obj_val #-}
get_obj_val :: (GLPK g , PrimMonad m) => g (PrimState m) -> m Double
get_obj_val g = withGLPK g glp_get_obj_val

{-# INLINE get_row_stat #-}
get_row_stat :: (GLPK g , PrimMonad m) => g (PrimState m) -> RVar -> m STAT
get_row_stat g r = withGLPK g \p -> glp_get_row_stat p (coerce r)

{-# INLINE get_row_prim #-}
get_row_prim :: (GLPK g , PrimMonad m) => g (PrimState m) -> RVar -> m Double
get_row_prim g r = withGLPK g \p -> glp_get_row_prim p (coerce r)

{-# INLINE get_row_dual #-}
get_row_dual :: (GLPK g , PrimMonad m) => g (PrimState m) -> RVar -> m Double
get_row_dual g r = withGLPK g \p -> glp_get_row_dual p (coerce r)

{-# INLINE get_col_stat #-}
get_col_stat :: (GLPK g , PrimMonad m) => g (PrimState m) -> CVar -> m STAT
get_col_stat g c = withGLPK g \p -> glp_get_col_stat p (coerce c)

{-# INLINE get_col_prim #-}
get_col_prim :: (GLPK g , PrimMonad m) => g (PrimState m) -> CVar -> m Double
get_col_prim g c = withGLPK g \p -> glp_get_col_prim p (coerce c)

{-# INLINE get_col_dual #-}
get_col_dual :: (GLPK g , PrimMonad m) => g (PrimState m) -> CVar -> m Double
get_col_dual g c = withGLPK g \p -> glp_get_col_dual p (coerce c)

{-# INLINE solve #-}
-- enable presolver / timeout (in ms)
solve :: (GLPK g , PrimMonad m) => g (PrimState m) -> Bool -> Int -> m S_ERR
solve g presolve timeout_ms = withGLPK g \p -> glpk_solve p
  do if presolve then 1 else 0
  do case timeout_ms of
       I# i -> CInt (I32# (intToInt32# i))

{-# INLINE assign_kmul #-}
assign_kmul :: (GLPK g , PrimMonad m) => g (PrimState m) -> CVar -> Double -> CVar -> m ()
assign_kmul g o k v = withGLPK g \p -> glpk_assign_kmul p o k v

{-# INLINE assign_add #-}
assign_add :: (GLPK g , PrimMonad m) => g (PrimState m) -> CVar -> CVar -> CVar -> m ()
assign_add g o a b = withGLPK g \p -> glpk_assign_add p o a b

{-# INLINE assert_rel #-}
assert_rel :: (GLPK g , PrimMonad m) => g (PrimState m) -> BD -> CVar -> CVar -> m ()
assert_rel g r a b = withGLPK g \p -> glpk_assert_rel p (coerce r) a b

newtype BD = BD CInt

pattern BD_FREE :: BD
pattern BD_FREE <- BD 1
  where
    BD_FREE = BD 1

pattern BD_LOWER :: BD
pattern BD_LOWER <- BD 2
  where
    BD_LOWER = BD 2

pattern BD_UPPER :: BD
pattern BD_UPPER <- BD 3
  where
    BD_UPPER = BD 3

pattern BD_BOTH :: BD
pattern BD_BOTH <- BD 4
  where
    BD_BOTH = BD 4

pattern BD_FIXED :: BD
pattern BD_FIXED <- BD 5
  where
    BD_FIXED = BD 5

{-# COMPLETE BD_FREE , BD_LOWER , BD_UPPER , BD_BOTH , BD_FIXED #-}

newtype OnOff = OnOff CInt

pattern Off :: OnOff
pattern Off <- OnOff 0
  where
    Off = OnOff 0

pattern On :: OnOff
pattern On <- OnOff 1
  where
    On = OnOff 1

{-# COMPLETE On , Off #-}

newtype STAT = STAT CInt

pattern STAT_BS :: STAT
pattern STAT_BS <- STAT 1
  where
    STAT_BS = STAT 1

pattern STAT_NL :: STAT
pattern STAT_NL <- STAT 2
  where
    STAT_NL = STAT 2

pattern STAT_NU :: STAT
pattern STAT_NU <- STAT 3
  where
    STAT_NU = STAT 3

pattern STAT_NF :: STAT
pattern STAT_NF <- STAT 4
  where
    STAT_NF = STAT 4

pattern STAT_NS :: STAT
pattern STAT_NS <- STAT 5
  where
    STAT_NS = STAT 5

{-# COMPLETE STAT_BS , STAT_NL , STAT_NU , STAT_NF , STAT_NS #-}

newtype S_ERR = S_ERR CInt

instance Show S_ERR where
  show S_ERR_OK = "Ok"
  show S_ERR_INVALID_BASIS = "Invalid basis"
  show S_ERR_SINGULAR_MATRIX = "Singular matrix"
  show S_ERR_ILL_CONDITIONED = "Ill-conditioned"
  show S_ERR_INVALID_BOUNDS = "Invalid bounds"
  show S_ERR_FAIL = "Fail"
  show S_ERR_OBJ_LL = "Objective function lower-limit reached"
  show S_ERR_OBJ_UL = "Objective function upper-limit reached"
  show S_ERR_ITER_LIM = "Iteration limit reached"
  show S_ERR_TIMEOUT = "Timeout"
  show S_ERR_NO_PRIMAL_FEASIBLE = "No primal feasible"
  show S_ERR_NO_DUAL_FEASIBLE = "No dual feasible"
  show S_ERR_ROOT = "Root"
  show S_ERR_STOP = "Stop"
  show S_ERR_MIPGAP = "Mipgap"
  show S_ERR_NO_PRIMAL_DUAL_FEASIBLE = "No primal-dual feasible"
  show S_ERR_NO_CONVERGENCE = "No convergence"
  show S_ERR_NUMERICAL_INSTABILITY = "Numerical instability"
  show S_ERR_INVALID_DATA = "Invalid data"
  show S_ERR_RANGE = "Range"

pattern S_ERR_OK :: S_ERR
pattern S_ERR_OK <- S_ERR 0x00
  where
    S_ERR_OK = S_ERR 0x00

pattern S_ERR_INVALID_BASIS :: S_ERR
pattern S_ERR_INVALID_BASIS <- S_ERR 0x01
  where
    S_ERR_INVALID_BASIS = S_ERR 0x01

pattern S_ERR_SINGULAR_MATRIX :: S_ERR
pattern S_ERR_SINGULAR_MATRIX <- S_ERR 0x02
  where
    S_ERR_SINGULAR_MATRIX = S_ERR 0x02

pattern S_ERR_ILL_CONDITIONED :: S_ERR
pattern S_ERR_ILL_CONDITIONED <- S_ERR 0x03
  where
    S_ERR_ILL_CONDITIONED = S_ERR 0x03

pattern S_ERR_INVALID_BOUNDS :: S_ERR
pattern S_ERR_INVALID_BOUNDS <- S_ERR 0x04
  where
    S_ERR_INVALID_BOUNDS = S_ERR 0x04

pattern S_ERR_FAIL :: S_ERR
pattern S_ERR_FAIL <- S_ERR 0x05
  where
    S_ERR_FAIL = S_ERR 0x05

pattern S_ERR_OBJ_LL :: S_ERR
pattern S_ERR_OBJ_LL <- S_ERR 0x06
  where
    S_ERR_OBJ_LL = S_ERR 0x06

pattern S_ERR_OBJ_UL :: S_ERR
pattern S_ERR_OBJ_UL <- S_ERR 0x07
  where
    S_ERR_OBJ_UL = S_ERR 0x07

pattern S_ERR_ITER_LIM :: S_ERR
pattern S_ERR_ITER_LIM <- S_ERR 0x08
  where
    S_ERR_ITER_LIM = S_ERR 0x08

pattern S_ERR_TIMEOUT :: S_ERR
pattern S_ERR_TIMEOUT <- S_ERR 0x09
  where
    S_ERR_TIMEOUT = S_ERR 0x09

pattern S_ERR_NO_PRIMAL_FEASIBLE :: S_ERR
pattern S_ERR_NO_PRIMAL_FEASIBLE <- S_ERR 0x0a
  where
    S_ERR_NO_PRIMAL_FEASIBLE = S_ERR 0x0a

pattern S_ERR_NO_DUAL_FEASIBLE :: S_ERR
pattern S_ERR_NO_DUAL_FEASIBLE <- S_ERR 0x0b
  where
    S_ERR_NO_DUAL_FEASIBLE = S_ERR 0x0b

pattern S_ERR_ROOT :: S_ERR
pattern S_ERR_ROOT <- S_ERR 0x0c
  where
    S_ERR_ROOT = S_ERR 0x0c

pattern S_ERR_STOP :: S_ERR
pattern S_ERR_STOP <- S_ERR 0x0d
  where
    S_ERR_STOP = S_ERR 0x0d

pattern S_ERR_MIPGAP :: S_ERR
pattern S_ERR_MIPGAP <- S_ERR 0x0e
  where
    S_ERR_MIPGAP = S_ERR 0x0e

pattern S_ERR_NO_PRIMAL_DUAL_FEASIBLE :: S_ERR
pattern S_ERR_NO_PRIMAL_DUAL_FEASIBLE <- S_ERR 0x0f
  where
    S_ERR_NO_PRIMAL_DUAL_FEASIBLE = S_ERR 0x0f

pattern S_ERR_NO_CONVERGENCE :: S_ERR
pattern S_ERR_NO_CONVERGENCE <- S_ERR 0x10
  where
    S_ERR_NO_CONVERGENCE = S_ERR 0x10

pattern S_ERR_NUMERICAL_INSTABILITY :: S_ERR
pattern S_ERR_NUMERICAL_INSTABILITY <- S_ERR 0x11
  where
    S_ERR_NUMERICAL_INSTABILITY = S_ERR 0x11

pattern S_ERR_INVALID_DATA :: S_ERR
pattern S_ERR_INVALID_DATA <- S_ERR 0x12
  where
    S_ERR_INVALID_DATA = S_ERR 0x12

pattern S_ERR_RANGE :: S_ERR
pattern S_ERR_RANGE <- S_ERR 0x13
  where
    S_ERR_RANGE = S_ERR 0x13

{-# COMPLETE S_ERR_OK , S_ERR_INVALID_BASIS , S_ERR_SINGULAR_MATRIX , S_ERR_ILL_CONDITIONED , S_ERR_INVALID_BOUNDS , S_ERR_FAIL , S_ERR_OBJ_LL , S_ERR_OBJ_UL , S_ERR_ITER_LIM , S_ERR_TIMEOUT , S_ERR_NO_PRIMAL_FEASIBLE , S_ERR_NO_DUAL_FEASIBLE , S_ERR_ROOT , S_ERR_STOP , S_ERR_MIPGAP , S_ERR_NO_PRIMAL_DUAL_FEASIBLE , S_ERR_NO_CONVERGENCE , S_ERR_NUMERICAL_INSTABILITY , S_ERR_INVALID_DATA , S_ERR_RANGE #-}

newtype SOL_STATUS = SOL_STATUS CInt

instance Show SOL_STATUS where
  show SOL_UNDEFINED = "Undefined"
  show SOL_FEASIBLE = "Feasible"
  show SOL_INFEASIBLE = "Infeasible"
  show SOL_NO_FEASIBLE = "No feasible"
  show SOL_OPTIMAL = "Optimal"
  show SOL_UNBOUNDED = "Unbounded"

pattern SOL_UNDEFINED :: SOL_STATUS
pattern SOL_UNDEFINED <- SOL_STATUS 1
  where
    SOL_UNDEFINED = SOL_STATUS 1

pattern SOL_FEASIBLE :: SOL_STATUS
pattern SOL_FEASIBLE <- SOL_STATUS 2
  where
    SOL_FEASIBLE = SOL_STATUS 2

pattern SOL_INFEASIBLE :: SOL_STATUS
pattern SOL_INFEASIBLE <- SOL_STATUS 3
  where
    SOL_INFEASIBLE = SOL_STATUS 3

pattern SOL_NO_FEASIBLE :: SOL_STATUS
pattern SOL_NO_FEASIBLE <- SOL_STATUS 4
  where
    SOL_NO_FEASIBLE = SOL_STATUS 4

pattern SOL_OPTIMAL :: SOL_STATUS
pattern SOL_OPTIMAL <- SOL_STATUS 5
  where
    SOL_OPTIMAL = SOL_STATUS 5

pattern SOL_UNBOUNDED :: SOL_STATUS
pattern SOL_UNBOUNDED <- SOL_STATUS 6
  where
    SOL_UNBOUNDED = SOL_STATUS 6

{-# COMPLETE SOL_UNDEFINED , SOL_FEASIBLE , SOL_INFEASIBLE , SOL_NO_FEASIBLE , SOL_OPTIMAL , SOL_UNBOUNDED #-}

