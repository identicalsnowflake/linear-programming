{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RoleAnnotations #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE ViewPatterns #-}

{-# OPTIONS_GHC -fno-warn-redundant-constraints #-}

-- | This is a low-level module. If you are simply trying to construct and solve a linear programming problem, it is needed only to import the GLPK top-level module, which gives an ergonomic and safe interface for doing so.

module LP.Problem where

import Data.Coerce
import Data.AffineSpace
import Data.VectorSpace
import Foreign.C.Types
import EShare.Hashable128

import LP.CBindings


-- | A linear expression. Addition and scalar multiplication are available using their respective operators from Data.VectorSpace (namely, @^+^@ and @*^@), which are re-exported for convenience. Alternatively, addition is can be expressed via the semigroup operator. Constants may be lifted via the @K@ pattern.
--
-- The final type variable is phantom, intended to permit lifting newtypes and their respectively delineated vector- and affine-space operations, and thus, permitting the lifting of domain-level type-safety into the declaration of the linear problem.
type role L phantom phantom
newtype L s a = L (E s)

{-# INLINE K #-}
pattern K :: Coercible a Double => a -> L s a
pattern K x <- L (Konst (coerce -> x))
  where
    K x = L (Konst (coerce x))

-- | Alias denoting terms for the objective function.
type Score = Double

data E s =
    Var {-# UNPACK #-} !CVar
  | Konst {-# UNPACK #-} !Double
  | KMul {-# UNPACK #-} !Double !(E s)
  | Add {-# UNPACK #-} !Hash128 !(E s) !(E s)

data LProb s = LProb {
    objf :: [ E s ]
  , constraints :: [ Rel s ]
  , basicVarCount :: {-# UNPACK #-} !CInt
  }

newtype Tri = Tri BD

{-# INLINE Eq #-}
pattern Eq :: Tri
pattern Eq <- Tri BD_FIXED
  where
    Eq = Tri BD_FIXED

{-# INLINE Lte #-}
pattern Lte :: Tri
pattern Lte <- Tri BD_UPPER
  where
    Lte = Tri BD_UPPER

{-# INLINE Gte #-}
pattern Gte :: Tri
pattern Gte <- Tri BD_LOWER
  where
    Gte = Tri BD_LOWER

-- | A trichotomous relation between two linear expressions.
data Rel s = Rel {-# UNPACK #-} !Tri !(E s) !(E s)

-- | Equality relation
infix 4 ===
{-# INLINE (===) #-}
(===) :: IsDouble a => L s a -> L s a -> Rel s
(===) x y = Rel Eq (coerce x) (coerce y)

-- | Less-than relation
infix 4 <==
{-# INLINE (<==) #-}
(<==) :: IsDouble a => L s a -> L s a -> Rel s
(<==) x y = Rel Lte (coerce x) (coerce y)

-- | Greater-than relation
infix 4 >==
{-# INLINE (>==) #-}
(>==) :: IsDouble a => L s a -> L s a -> Rel s
(>==) x y = Rel Gte (coerce x) (coerce y)

-- | A phantom typeclass with the law that if an instance exists for @a@ for any of the following typeclasses, they must respect the newtype-derived instance via @Double@, modulo coercions:
-- 
-- 1. Num
-- 2. AdditiveGroup
-- 3. VectorSpace
-- 4. AffineSpace
-- 5. Eq
-- 6. Ord
--
-- This ensures that the operations inside the linear programming solver respect their corresponding operations as defined in Haskell.

class Coercible a Double => IsDouble a
instance IsDouble Double

instance Hashable128 (E s) where
  {-# INLINABLE hash128 #-}
  hash128 (Var v) = hash128 v
  hash128 (Konst k) = hash128 k
  hash128 (KMul k x) = Hash128 4526199951262499479 2599140310031281077 <+> hash128 k <+> hash128 x
  hash128 (Add h _ _) = h

instance AdditiveGroup (E s) where
  {-# INLINE (^+^) #-}
  (^+^) (Konst x) (Konst y) = Konst (x + y)
  (^+^) x y = Add (Hash128 (-9189325436942452706) (-4455458234449006132) <+> hash128 x <+> hash128 y) x y
  {-# INLINE negateV #-}
  negateV (Konst k) = Konst (-1 * k)
  negateV (KMul k x) = KMul (-1 * k) x
  negateV x = KMul (-1) x
  {-# INLINE zeroV #-}
  zeroV = Konst 0

instance VectorSpace (E s) where
  type Scalar (E s) = Double
  {-# INLINE (*^) #-}
  (*^) k (Konst x) = Konst (k * x)
  (*^) k (KMul x v) = KMul (k * x) v
  (*^) k x = KMul k x

instance (AdditiveGroup a , IsDouble a) => AdditiveGroup (L s a) where
  {-# INLINE (^+^) #-}
  (^+^) (L x) (L y) = L (x ^+^ y)
  {-# INLINE negateV #-}
  negateV (L x) = L (negateV x)
  {-# INLINE zeroV #-}
  zeroV = L zeroV

instance (VectorSpace a , IsDouble a , IsDouble (Scalar a)) => VectorSpace (L s a) where
  type Scalar (L s a) = Scalar a
  {-# INLINE (*^) #-}
  (*^) k (L x) = L (coerce k *^ x)

instance (AffineSpace a , IsDouble a , IsDouble (Diff a)) => AffineSpace (L s a) where
  type Diff (L s a) = L s (Diff a)
  {-# INLINE (.+^) #-}
  (.+^) (L x) (L o) = L (x ^+^ o)
  {-# INLINE (.-.) #-}
  (.-.) (L x) (L y) = L (x ^+^ negateV y)


instance Semigroup (E s) where
  {-# INLINE (<>) #-}
  (<>) = (^+^)

instance Monoid (E s) where
  {-# INLINE mempty #-}
  mempty = zeroV

