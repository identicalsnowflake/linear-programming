{-# LANGUAGE RecordWildCards #-}

module LP.Encoding (encodeGLPK) where

import Control.Monad
import Control.Monad.ST
import Data.Coerce
import Data.Foldable
import Data.Traversable
import Foreign.C.Types
import qualified EShare as ES
import EShare.Hashable128

import LP.CBindings
import LP.Problem


{-# INLINABLE encodeGLPK #-}
-- | Translate and encode the high-level linear programming problem statement into the given GLPK instance. The GLPK instance should be empty (i.e., either freshly initialized or erased).
encodeGLPK :: (GLPK g , Traversable t) => t (E a) -> LProb a -> g s -> ST s (t CVar)
encodeGLPK vs LProb { .. } p = do
  
  dd <- ES.new 8

  for_ vs (dedup dd)
  for_ objf (dedup dd)
  for_ constraints \case
    Rel _ e1 e2 -> dedup dd e1 >> dedup dd e2

  c <- ES.cardinality dd
  ass <- ES.switchModes dd (1 + coerce basicVarCount :: CInt)


  let tvc :: Int
      tvc = fromIntegral (coerce basicVarCount :: CInt) + c
  
  when (tvc > 0) do
    add_cols p tvc
    pure ()

  for_ [ (1 :: CInt) .. fromIntegral tvc ] \i ->
    set_col_bounds p (coerce i) BD_FREE 0 0

  vs' <- for vs (assignSub ass p)
  for_ objf \x -> do
    ix <- assignSub ass p x
    k <- get_obj_coef p ix
    set_obj_coef p ix (1.0 + k)
  for_ constraints \case
    Rel rel e1 e2 -> do
      ia <- assignSub ass p e1
      ib <- assignSub ass p e2

      -- necessary because glpk will outright crash if these are the same, despite this being
      -- a harmless, redundant constraint
      when (ia /= ib) do
        assert_rel p (coerce rel) ia ib
    

  ES.free ass

  pure vs'

{-# INLINABLE assignSub #-}
-- | Assign an expression to be the result of the computation of its sub-expressions
assignSub :: forall s a g . GLPK g => ES.Assigner s -> g s -> E a -> ST s CVar
assignSub ass p = \e -> do
  go e
  where
    go :: E a -> ST s CVar
    go (Var v) = pure v
    go o@(Konst k) = assignFresh ass o \oid -> do
      set_col_bounds p oid BD_FIXED k 0
    go o@(KMul k i) = assignFresh ass o \oid -> do
      iid <- go i
      assign_kmul p oid k iid
    go o@(Add _ a b) = assignFresh ass o \oid -> do
      aid <- go a
      bid <- go b
      assign_add p oid aid bid

{-# INLINE assignFresh #-}
assignFresh :: ES.Assigner s -> E a -> (CVar -> ST s ()) -> ST s CVar
assignFresh ass e f = ES.assignKnown ass (hash128 e) >>= \i -> do
  when (i > 0) (f (coerce i))
  pure (coerce (abs i))

-- | Add an expression and its sub-expressions to the deduplicator
dedup :: ES.DeDuper s -> E a -> ST s ()
dedup dd = go
  where
    go (Var _) = pure ()
    go e@(Konst _) = ES.insert dd (hash128 e) >> pure ()
    go e@(KMul _ e') = ES.insert dd (hash128 e) >>= \case
      0 -> pure ()
      _ -> go e'
    go (Add h e1 e2) = ES.insert dd h >>= \case
      0 -> pure ()
      _ -> go e1 >> go e2

