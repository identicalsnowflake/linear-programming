{-# LANGUAGE UndecidableInstances #-}

module LP.Monad where

import qualified Control.Monad.Trans.Class as T
import qualified Control.Monad.Trans.Cont
import qualified Control.Monad.Trans.Except
import qualified Control.Monad.Trans.Identity
import qualified Control.Monad.Trans.Reader as R
import qualified Control.Monad.Trans.RWS.Lazy
import qualified Control.Monad.Trans.RWS.Strict
import qualified Control.Monad.Trans.Select
import qualified Control.Monad.Trans.State.Lazy as LS
import qualified Control.Monad.Trans.State.Strict as S
import qualified Control.Monad.Trans.Writer.CPS
import qualified Control.Monad.Trans.Writer.Lazy
import qualified Control.Monad.Trans.Writer.Strict

import Data.Coerce

import LP.CBindings
import LP.Problem


-- | Constraint-only monad. Can be useful in situations where one part of the code should only generate constraints for given variables, lacking the freedom to allocate its own basic variables or touch the objective function.
class Monad m => LCMonad s m | m -> s where
  assert :: Rel s -> m ()

-- | High-level monad for stating linear programming problems.
class LCMonad s m => LMonad s m where
  newVar :: IsDouble a => m (L s a)
  addObjective :: L s Score -> m ()

newtype LCM s a = LCM (LS.State (LProb s) a)
  deriving newtype (Functor,Applicative,Monad)

{-# INLINE getProblem #-}
getProblem :: LCM s a -> (a , LProb s)
getProblem = flip LS.runState (LProb mempty mempty 0) . coerce

instance LCMonad s (LCM s) where
  {-# INLINE assert #-}
  assert !c = LCM do LS.modify \s -> s { constraints = c : constraints s }

instance LMonad s (LCM s) where
  {-# INLINE newVar #-}
  newVar = LCM do
    s <- LS.get
    let v = basicVarCount s + 1
    let !s' = s { basicVarCount = v }
    LS.put s'
    pure (L (Var (coerce v)))
  {-# INLINE addObjective #-}
  addObjective !(L e) = LCM do LS.modify \s -> s { objf = e : objf s }


---------------------
--  MTL instances  --
---------------------

instance LCMonad s m => LCMonad s (R.ReaderT e m) where
  {-# INLINE assert #-}
  assert = T.lift . assert

instance LCMonad s m => LCMonad s (S.StateT e m) where
  {-# INLINE assert #-}
  assert = T.lift . assert

instance LCMonad s m => LCMonad s (LS.StateT e m) where
  {-# INLINE assert #-}
  assert = T.lift . assert

instance LCMonad s m => LCMonad s (Control.Monad.Trans.Cont.ContT e m) where
  {-# INLINE assert #-}
  assert = T.lift . assert

instance LCMonad s m => LCMonad s (Control.Monad.Trans.Except.ExceptT e m) where
  {-# INLINE assert #-}
  assert = T.lift . assert

instance LCMonad s m => LCMonad s (Control.Monad.Trans.Identity.IdentityT m) where
  {-# INLINE assert #-}
  assert = T.lift . assert

instance (Monoid w , LCMonad s m) => LCMonad s (Control.Monad.Trans.RWS.Lazy.RWST r w j m) where
  {-# INLINE assert #-}
  assert = T.lift . assert

instance (Monoid w , LCMonad s m) => LCMonad s (Control.Monad.Trans.RWS.Strict.RWST r w j m) where
  {-# INLINE assert #-}
  assert = T.lift . assert

instance LCMonad s m => LCMonad s (Control.Monad.Trans.Select.SelectT e m) where
  {-# INLINE assert #-}
  assert = T.lift . assert

instance LCMonad s m => LCMonad s (Control.Monad.Trans.Writer.CPS.WriterT w m) where
  {-# INLINE assert #-}
  assert = T.lift . assert

instance (Monoid w , LCMonad s m) => LCMonad s (Control.Monad.Trans.Writer.Lazy.WriterT w m) where
  {-# INLINE assert #-}
  assert = T.lift . assert

instance (Monoid w , LCMonad s m) => LCMonad s (Control.Monad.Trans.Writer.Strict.WriterT w m) where
  {-# INLINE assert #-}
  assert = T.lift . assert


instance LMonad s m => LMonad s (R.ReaderT e m) where
  {-# INLINE newVar #-}
  newVar = T.lift newVar
  {-# INLINE addObjective #-}
  addObjective = T.lift . addObjective

instance LMonad s m => LMonad s (S.StateT e m) where
  {-# INLINE newVar #-}
  newVar = T.lift newVar
  {-# INLINE addObjective #-}
  addObjective = T.lift . addObjective

instance LMonad s m => LMonad s (LS.StateT e m) where
  {-# INLINE newVar #-}
  newVar = T.lift newVar
  {-# INLINE addObjective #-}
  addObjective = T.lift . addObjective

instance LMonad s m => LMonad s (Control.Monad.Trans.Cont.ContT e m) where
  {-# INLINE newVar #-}
  newVar = T.lift newVar
  {-# INLINE addObjective #-}
  addObjective = T.lift . addObjective

instance LMonad s m => LMonad s (Control.Monad.Trans.Except.ExceptT e m) where
  {-# INLINE newVar #-}
  newVar = T.lift newVar
  {-# INLINE addObjective #-}
  addObjective = T.lift . addObjective

instance LMonad s m => LMonad s (Control.Monad.Trans.Identity.IdentityT m) where
  {-# INLINE newVar #-}
  newVar = T.lift newVar
  {-# INLINE addObjective #-}
  addObjective = T.lift . addObjective

instance (Monoid w , LMonad s m) => LMonad s (Control.Monad.Trans.RWS.Lazy.RWST r w j m) where
  {-# INLINE newVar #-}
  newVar = T.lift newVar
  {-# INLINE addObjective #-}
  addObjective = T.lift . addObjective

instance (Monoid w , LMonad s m) => LMonad s (Control.Monad.Trans.RWS.Strict.RWST r w j m) where
  {-# INLINE newVar #-}
  newVar = T.lift newVar
  {-# INLINE addObjective #-}
  addObjective = T.lift . addObjective

instance LMonad s m => LMonad s (Control.Monad.Trans.Select.SelectT e m) where
  {-# INLINE newVar #-}
  newVar = T.lift newVar
  {-# INLINE addObjective #-}
  addObjective = T.lift . addObjective

instance LMonad s m => LMonad s (Control.Monad.Trans.Writer.CPS.WriterT w m) where
  {-# INLINE newVar #-}
  newVar = T.lift newVar
  {-# INLINE addObjective #-}
  addObjective = T.lift . addObjective

instance (Monoid w , LMonad s m) => LMonad s (Control.Monad.Trans.Writer.Lazy.WriterT w m) where
  {-# INLINE newVar #-}
  newVar = T.lift newVar
  {-# INLINE addObjective #-}
  addObjective = T.lift . addObjective

instance (Monoid w , LMonad s m) => LMonad s (Control.Monad.Trans.Writer.Strict.WriterT w m) where
  {-# INLINE newVar #-}
  newVar = T.lift newVar
  {-# INLINE addObjective #-}
  addObjective = T.lift . addObjective

