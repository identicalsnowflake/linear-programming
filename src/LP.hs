{-# LANGUAGE PatternSynonyms #-}

-- | EDSL for [linear programming](https://en.wikipedia.org/wiki/Linear_programming). Example usage:
--
-- @
-- import LP
-- 
-- problem :: LMonad s m => m [ L s ]
-- problem = do
--   a <- newVar
--   b <- newVar
-- 
--   assert $ a >== K 2
--   assert $ a <== K 4
--   
--   assert $ 2 *^ a ^+^ b >== K 5
-- 
--   addObjective $ a
--   addObjective $ b
-- 
--   pure [ a , b ]
-- 
-- main :: IO ()
-- main = print $ minimize problem -- Just [4.0,-3.0]
-- @

module LP
  ( minimize
  , r2minimize
  , LMonad(..)
  , LCMonad(..)
  , type L
  , pattern K
  , type Rel
  , (===)
  , (<==)
  , (>==)
  , IsDouble
    -- * Utility re-exports
  , AdditiveGroup(..)
  , AffineSpace(..)
  , VectorSpace(..)
  ) where

import Control.Monad
import Control.Monad.ST
import Data.Coerce
import Data.AffineSpace
import Data.VectorSpace

import Barbies
import qualified Control.Monad.Trans.State as S
import Data.Functor.Identity

import LP.CBindings
import LP.Encoding (encodeGLPK)
import LP.Problem
import LP.Monad


{-# INLINE minimize #-}
-- | Find the solution that minimizes the objective function for the given linear programming problem; if none exists, return @Nothing@.
minimize :: Traversable t => (forall s m . LMonad s m => m (t (L s Double))) -> Maybe (t Double)
minimize p = do
  let (vs , lp) = getProblem p
  minimize' lp (coerce <$> vs)

{-# INLINE r2minimize #-}
-- | Rank-2 version of minimize, permitting easy lifting of domain newtypes and their relations via vector- and affine-space operations through the linear programming vocabulary.
r2minimize :: (TraversableB t , ConstraintsB t , AllB IsDouble t)
           => (forall s m . LMonad s m => m (t (L s))) -> Maybe (t Identity)
r2minimize p = do
  let (vs,lp) = getProblem p
  r2reify vs . fmap Identity <$> minimize' lp do
    reverse do
      flip S.execState [] do
        btraverse_ (\(L e) -> S.modify (e:)) vs

{-# INLINABLE r2reify #-}
r2reify :: (TraversableB t , ConstraintsB t , AllB IsDouble t)
        => t f -> [ Identity Double ] -> t Identity
r2reify tr = S.evalState (btraverseC @IsDouble g tr)
  where
    g :: IsDouble w => a -> S.State [ Identity Double ] (Identity w)
    g _ = S.get >>= \case
        (x:xs) -> do
          S.put xs
          return (coerce x)
        [] -> error "Impossible in r2reify: size mismatch"

{-# INLINABLE minimize' #-}
minimize' :: Traversable t => LProb CVar -> t (E CVar) -> Maybe (t Double)
minimize' lp vs = runST do
  
  p <- newUnmanagedGLPK

  vs' <- encodeGLPK (coerce <$> vs) lp p

  -- glpk will crash if given the empty problem, so throw in this incantation
  when (basicVarCount lp == 0) do
    add_cols p 1
    pure ()
  
  solve p True 2147483647 >>= \case
    S_ERR_OK -> get_status p >>= \case
      SOL_OPTIMAL -> Just <$> do
        r <- traverse (get_col_prim p) vs'
        freeUnmanaged p
        pure r
      SOL_FEASIBLE -> Just <$> do
        r <- traverse (get_col_prim p) vs'
        freeUnmanaged p
        pure r
      _ -> do
        freeUnmanaged p
        pure Nothing
    _ -> do
      freeUnmanaged p
      pure Nothing

