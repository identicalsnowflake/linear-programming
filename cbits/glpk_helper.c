#include "glpk_helper.h"
#include <limits.h>


extern int glpk_solve(glp_prob* p, int presolve, int timeout_ms) {
  glp_smcp s;
  glp_init_smcp(&s);
  s.msg_lev = GLP_MSG_OFF;
  s.out_frq = INT_MAX;
  s.meth = GLP_DUALP;
  s.tm_lim = timeout_ms;
  s.presolve = presolve;
  return glp_simplex(p,&s);
}

extern inline void glpk_assign_kmul(glp_prob* p, int o, double c, int v) {
  int vs[3];
  double cs[3];
  
  cs[1] = c;
  vs[1] = v;
  
  cs[2] = -1.0;
  vs[2] = o;

  int r = glp_add_rows(p,1);
  glp_set_mat_row(p,r,2,vs,cs);
  glp_set_row_bnds(p,r,GLP_FX,0,0);
}

extern inline void glpk_assign_add(glp_prob* p, int o, int a, int b) {
  int vs[4];
  double cs[4];
  
  cs[1] = 1.0;
  vs[1] = a;
  
  cs[2] = 1.0;
  vs[2] = b;
  
  cs[3] = -1.0;
  vs[3] = o;

  int r = glp_add_rows(p,1);
  glp_set_mat_row(p,r,3,vs,cs);
  glp_set_row_bnds(p,r,GLP_FX,0,0);
}

extern inline void glpk_assert_rel(glp_prob* p, int rel, int a , int b) {

  int vs[3];
  double cs[3];

  vs[1] = a;
  vs[2] = b;

  cs[1] = 1.0;
  cs[2] = -1.0;

  int r = glp_add_rows(p,1);
  glp_set_mat_row(p,r,2,vs,cs);
  glp_set_row_bnds(p,r,rel,0,0);
}

