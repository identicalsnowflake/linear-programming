#ifndef GLPK_HELPER_H
#define GLPK_HELPER_H

#include "./../glpk/build/include/glpk.h"

extern int glpk_solve(glp_prob* p, int presolve, int timeout_ms);
extern inline void glpk_assign_kmul(glp_prob* p, int o, double c, int v);
extern inline void glpk_assign_add(glp_prob* p, int o, int a, int b);
extern inline void glpk_assert_rel(glp_prob* p, int rel, int a , int b);

#endif

