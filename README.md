# Linear Programming

This library provides an EDSL for solving linear programming problems (currently via glpk). Example use:

```haskell
module Main where

import LP


problem :: LMonad s m => m [ L s Double ]
problem = do

  a <- newVar
  b <- newVar

  assert $ a >== K 1
  assert $ b >== K 1

  assert $ 2 *^ (a ^+^ b) >== 6 *^ b

  addObjective $ a
  addObjective $ b

  pure [ a , b ]

main :: IO ()
main = print $ minimize problem
```

